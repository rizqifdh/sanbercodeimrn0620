//Tugas Conditional (If-Else)
var nama = "Jarwo";
var peran = "Guard";

if(nama == "" && peran == "" ) {
    console.log("Nama harus diisi!")
}else if (nama != "" && peran == "") {
	console.log("Halo " + nama + ", Pilih peranmu untuk memulai game!")
}else if (nama != "" && peran == "Guard"){
	console.log("Halo Guard "+nama+", kamu akan membantu melindungi temanmu dari serangan werewolf.")
}else if (nama != "" && peran == "Penyihir"){
	console.log("Halo Penyihir "+nama+", kamu dapat melihat siapa yang menjadi werewolf!")
}else if (nama != "" && peran == "Werewolf"){
	console.log("Halo Werewolf "+nama+", Kamu akan memakan mangsa setiap malam!")
}