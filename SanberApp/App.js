import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import Quiz3 from './Tugas/Quiz3/index';
import LoginScreen from './Tugas/Quiz3/LoginScreen';
import HomeScreen from './Tugas/Quiz3/HomeScreen';
import ToDoUI from './Tugas/Tugas14/App';


//export default function App() {
const App = () =>{
  return (
    <Quiz3/>
    //<ToDoUI/>
    //<LoginScreen/>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default App;