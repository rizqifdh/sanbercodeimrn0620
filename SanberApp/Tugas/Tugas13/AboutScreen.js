import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    TextInput,
    TouchableOpacity,
    FlatList,
    Button,
    SafeAreaView,
    Alert
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Icons from 'react-native-vector-icons/MaterialCommunityIcons';


export default class App extends Component {
    render() {        
        return (
            <View style={styles.container}>
                <Text style={styles.title}>Tentang Saya</Text>
                <Image source={{uri: 'https://pbs.twimg.com/profile_images/1249604660148633601/d4JJ6BYF_400x400.jpg'}} style={{width:180, height: 180, borderRadius: 90}}/>
                <Text style={styles.title1}>Ismail bin Mail</Text>
                <Text style={styles.title2}>React Native Developer</Text>

                <View style={styles.portfolio}>
                    <View style={styles.portoView}>
                    <Text style={styles.title3}>Portofolio</Text>
                    </View>
                    <View style={styles.app}>
                        <View style={styles.logo1}>
                            <Icons style={styles.gitLab} name="gitlab" size={50}/>
                            <Text style={styles.title4}>@mukhliish</Text>
                        </View>

                        <View style={styles.logo2}>
                            <Icons style={styles.gitHub} name="github-circle" size={50}/>
                            <Text style={styles.title4}>@mukhlis-h</Text>
                        </View>
                    </View>
                </View>

                <View style={styles.media}>
                    <View style={styles.mediaView}>
                        <Text style={styles.title5}>Hubungi Saya</Text>
                    </View>
                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        paddingVertical: 100,
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center'
    },
    title: {
        height: 55,
        color: '#003366',
        fontSize: 30,
        fontWeight: 'bold'
    }, 
    title1: {
        marginTop: 10,
        fontSize: 24,
        color: '#003366',
        fontWeight: 'bold'
    },
    title2: {
        fontSize: 16,
        color: '#3EC6FF',
        fontWeight: 'bold'
    },
    title3: {
        marginTop: 10,
        color: '#003366',
        fontSize: 14,
        textDecorationLine: 'underline'
    },
    portfolio: {
        backgroundColor: '#EFEFEF',
        marginHorizontal: 15,
        marginTop: 15,
        paddingHorizontal: 100
    },
    app: {
        flexDirection: 'row',
        alignItems: 'center'
    }
});