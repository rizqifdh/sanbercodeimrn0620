import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    TextInput,
    TouchableOpacity,
    FlatList,
    Button,
    SafeAreaView,
    Alert
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';


export default class App extends Component {
    constructor() {
        super();
        this.state = {
          value: '',
        };
      }
    
    render() {
        
        return (
            <View style={styles.container}>
                <View style={styles.navBar}>
                    <Image source={require('../../images/logo1.png')} style={{width:300, height: 100}}/>
                </View>
                <View style={styles.login}>
                    <Text style={styles.title}>Login</Text>
                </View> 

                <View style={styles.form}> 
                    <View style={styles.input}>
                        <View style={styles.navInput}>
                        <Text style={styles.inputText}>Username / Email</Text>
                        <View>
                            <TextInput style={{ height: 45, width:320, borderColor: '#003366', borderWidth: 1 }} onChangeText={(text) => this.setState({ value: text })} value={this.state.value} />
                        </View>
                        </View>

                        <View style={styles.navInput}>
                        <Text style={styles.inputText}>Password</Text>
                        <View>
                            <TextInput style={{ height: 45, width:320, borderColor: '#003366', borderWidth: 1 }} onChangeText={(text) => this.setState({ value: text })} value={this.state.value} />
                        </View>
                        </View>
                    </View>
                </View>

                <TouchableOpacity>
                <View style={styles.btnLogin}>
                    <Text style={styles.btnText}>Masuk</Text>
                </View>
                </TouchableOpacity>

                <Text style={styles.atau}>atau</Text>

                <TouchableOpacity>
                <View style={styles.btnRegister}>
                    <Text style={styles.btnText}>Daftar ?</Text>
                </View>
                </TouchableOpacity>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        paddingVertical: 100,
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center'
    },
    navBar: {
        height: 55,
        backgroundColor: 'white'
    },
    login: {
        flexDirection: 'column',
        paddingTop: 60
    },
    title: {
        fontSize: 24,
        color: '#003366'
    },
    form: {
        marginTop: 35,
        marginHorizontal: 35,
        alignSelf: 'flex-start'
    },
    inputText: {
        color: '#003366',
        paddingVertical: 3,
        marginTop: 10
    },
    btnLogin: {
        marginTop: 30,
        paddingVertical: 8,
        paddingHorizontal: 30,
        backgroundColor: '#3EC6FF',
        borderRadius: 25
    },
    btnRegister: {
        marginTop: 10,
        paddingVertical: 8,
        paddingHorizontal: 30,
        backgroundColor: '#003366',
        borderRadius: 25
    },
    btnText: {
        color: 'white',
        fontSize: 20
    },
    atau: {
        fontSize: 18,
        color: '#3EC6FF',
        paddingVertical: 10
    }
});