import React from 'react';
import { View, Image, Text, ScrollView, TextInput, TouchableOpacity, StyleSheet } from 'react-native';


export default class Note extends React.Component {
  render() {
    return (

      <View style={styles.container}>
          <View style={styles.wrapper}>
            <View style={styles.rightNav}>
              <Image source={require('../../images/logo1.png')} style={{width:150, height: 50}}/>
            </View>
          </View>
      </View>

    )
  }
};

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#aaaaaa'
    },
    wrapper: {
        margin: 5,
        backgroundColor: '#fff'
    }
  });
  
  