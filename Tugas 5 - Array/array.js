//Soal No 1 (Range)
console.log("Jawaban Soal No 1: ");
function range(startNum, finishNum) {
	var number = [];
	if (startNum < finishNum) {
	  for (var a=finishNum; a>=startNum; a--) {
	    number.unshift(a);
	  }
	  var hasil = number;
	}else if (startNum > finishNum) {
	  for (var a=finishNum; a<=startNum; a++) {
	    number.unshift(a);
	  }
	  var hasil = number;
	}else if (startNum == null || finishNum == null) {
	  var hasil = -1
	}
	return hasil;
};

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1
console.log(" ");

//Soal No 2 (Range with Step)
console.log("Jawaban Soal No 2: ");
function rangeWithStep(startNum, finishNum, step) {
	var angka = [];
	if (startNum < finishNum) {
	  for (var a=startNum; a<=finishNum; a+=step) {
	    angka.push(a);
	  }
	}else if (startNum > finishNum) {
	  for (var a=startNum; a>=finishNum; a-=step) {
	    angka.push(a);
	  }
	}
	return angka;
};

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 
console.log(" ");

//Soal No 3 (Sum of Range)
console.log("Jawaban Soal No 3: ");
function sum(startRow, finishRow, dif) {
	var deret = [];
	var hasil;
	if (startRow < finishRow && dif != null) {
	  for (var a=startRow; a<=finishRow; a+=dif) {
	    deret.push(a);
	  }
	  var sum = deret.reduce(function(a, b){return a + b;}, 0);
	  hasil = sum;
	}
	else if (startRow > finishRow && dif != null) {
	  for (var a=startRow; a>=finishRow; a-=dif) {
	    deret.push(a);
	  }
	  var sum = deret.reduce(function(a, b){return a + b;}, 0);
      hasil = sum;
	}
	else if (startRow < finishRow && dif == null) {
	  dif = 1;
	  for (var a=startRow; a<=finishRow; a+=dif) {
	    deret.push(a);
	  }
	  var sum = deret.reduce(function(a, b){return a + b;}, 0);
	  hasil = sum;
	}
	else if (startRow > finishRow && dif == null) {
	  dif = 1;
	  for (var a=startRow; a>=finishRow; a-=dif) {
	    deret.push(a);
	  }
	  var sum = deret.reduce(function(a, b){return a + b;}, 0);
      hasil = sum;
	}
	else if (finishRow == null && dif == null) {
	  dif = 1;
	  finishRow = 0;
	  for (var a=startRow; a>=finishRow; a-=dif) {
	    deret.push(a);
	  }
	  var sum = deret.reduce(function(a, b){return a + b;}, 0);
      hasil = sum;
	}
	else if (startRow == null && finishRow == null && dif == null) {
	  dif = 1;
	  startRow = 0;
	  finishRow = 0;
	  for (var a=startRow; a>=finishRow; a-=dif) {
	    deret.push(a);
	  }
	  var sum = deret.reduce(function(a, b){return a + b;}, 0);
      hasil = sum;
	}
	return hasil;
};

console.log(sum(1,10)); // 55
console.log(sum(5, 50, 2)); // 621
console.log(sum(15,10)); // 75
console.log(sum(20, 10, 2)); // 90
console.log(sum(1)); // 1
console.log(sum());
console.log(" ");

//Soal No 4 (Array Multidimensi)
console.log("Jawaban Soal No 4: ");
var input1 = [
	["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
	["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
	["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
	["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
            ]
var txt = "";
function dataHandling(input) {
  for (let i = 0; i < input1.length; i++) {
    for (let j=0; j<input1[i].length; j++) {
      if (j==0){
        txt += ("Nomor ID: "+ input1[i][j])+ "\n";
      }else if(j==1){
        txt += ("Nama Lengkap: "+ input1[i][j])+"\n";
      }else if(j==2){
        txt += ("TTL: "+ input1[i][j]);
      }else if(j==3){
        txt += (" "+ input1[i][j])+"\n";
      }else if(j==4){
        txt += ("Hobi: "+ input1[i][j]+"\n\n");
      }
    }
  }
  return txt;
}
console.log(dataHandling(input1));
console.log(" ");

//Soal No 5 (Balik Kata)
console.log("Jawaban Soal No 5: ")
function balikKata(word) {
  var string = '';
  for (let i=0; i<1; i++){
	for(let j=word.length-1; j>=0; j--){
		string += word[j]
	}
}
  return string;
}

console.log(balikKata("Kasur Rusak")); // kasuR rusaK
console.log(balikKata("SanberCode")); // edoCrebnaS
console.log(balikKata("Haji Ijah")); // hajI ijaH
console.log(balikKata("racecar")); // racecar
console.log(balikKata("I am Sanbers")); // srebnaS ma I
console.log(" ");

//Soal No 6 (Metode Array)
console.log("Jawaban Soal No 6: ");
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];

function dataHandling2(input) {
  input[1] = "Roman Alamsyah Elsharawy";
  input[2] = "Provinsi Bandar Lampung";
  input.splice(4, 1);
  input.splice(4, 0, "Pria", "SMA International Metro");
  console.log(input);
  
  tanggal = input[3].split("/");

  switch(tanggal[1]){
	case '01' :
		console.log("Januari");
		break;
	case '02' :
		console.log("Februari");
		break;
	case '03' :
		console.log("Maret");
		break;
	case '04' :
		console.log("April");
		break;
	case '05' :
		console.log("Mei");
		break;
	case '06' :
		console.log("Juni");
		break;
	case '07' :
		console.log("Juli");
		break;
	case '08' :
		console.log("Agustus");
		break;
	case '09' :
		console.log("September");
		break;
	case '10' :
		console.log("Oktober");
		break;
	case '11' :
		console.log("November");
		break;
	case '12' :
		console.log("Desember");
		break;
  }
  
  var gabung = tanggal.join("-");
  var sort = tanggal.sort(function (value1,value2) {return parseInt(value2) - parseInt(value1)});
  console.log(sort);
  
  console.log(gabung);
  
  nickname = input[1].slice(0,14);
  console.log(nickname);
  
}

dataHandling2(input);
console.log(" ");
