//Soal 1 (Looping While)
let forward=2;
console.log("LOOPING PERTAMA")
while (forward<=20) {
	console.log(forward+" - I love coding");
	forward +=2;
}
let backward=20;
console.log("LOOPING KEDUA")
while (backward>=2) {
	console.log(backward+" - I will become a mobile developer");
	backward -=2;
}
console.log(" ")

//Soal 2 (Looping For)
for (let number=1; number<=20; number++) {
  if (number%3==0 && number%2==1) {
     output = "I Love Coding";
     console.log(number+" - "+output);
  }else if (number%2==1) {
     output = "Santai";
     console.log(number+" - "+output);
  }else if (number%2==0) {
     output = "Berkualitas";
     console.log(number+" - "+output);
  }
}
console.log(" ")

//Soal 3 (Membuat Persegi Panjang)
var string = '';
for (let i=0; i<4; i++){
	for(let j=0; j<8; j++){
		string += "#"
	}
	string += "\n"
} console.log(string);

//Soal 4 (Membuat Tangga)
var tangga = '';
for (let i=1; i<=7; i++){
	for(let j=0; j<i; j++){
		tangga += "#"
	}
	tangga += "\n"
} console.log(tangga);

//Soal 5 (Membuat Papan Catur)
var catur = '';
for (let i=0; i<8; i++){
	for(let j=0; j<4; j++){
		if (i%2==0) {
			catur += " ";
			catur += "#";
		}else if (i%2==1) {
			catur += "#";
			catur += " ";
		}
	}
	catur += "\n"
} console.log(catur);