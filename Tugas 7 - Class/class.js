//Soal No 1 (Animal Class)
//RELEASE 0
class Animal {
	// Code class di sini
	constructor(nama) {
        this.nama = nama;
        this.legs = 4;
		this.cold_blooded = false;
	}

	get name() {
		return this.nama;
	}
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

//RELEASE 1
// Code class Ape dan class Frog di sini
class Ape extends Animal {
	constructor(nama) {
		super(nama);
        this.teriak = "Auooo";
	}

	yell() {
		console.log(this.teriak);
	}
}

class Frog extends Animal {
	constructor(nama) {
		super(nama);
        this.lompat = "hop hop";
	}

	jump() {
		console.log(this.lompat);
	}
}

var sungokong = new Ape("kera sakti");
sungokong.yell(); // "Auooo"
 
var kodok = new Frog("buduk");
kodok.jump(); // "hop hop"

//Soal No 2 (Function to Class)
class Clock {
	// Code di sini
	constructor({ template }){
		this.template = template;
	}

	render() {
	  this.date = new Date();
		
		this.hours = this.date.getHours();
		if (this.hours < 10) this.hours = '0' + this.hours;

    this.mins = this.date.getMinutes();
		if (this.mins < 10) this.mins = '0' + this.mins;
		
		this.secs = this.date.getSeconds();
		if (this.secs < 10) this.secs = '0' + this.secs;

		this.output = this.template.replace('h', this.hours).replace('m', this.mins).replace('s', this.secs);
		console.log(this.output);
	}

	stop() {
		clearInterval(this.timer);
	}

	start() {
		this.render();
		this.timer = setInterval(() => this.render(), 1000);
	}

}

var clock = new Clock({template: 'h:m:s'});
clock.start();