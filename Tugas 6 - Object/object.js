//Soal No 1 (Array to Object)
var now = new Date();
var thisYear = now.getFullYear();
var obj = "";
var objek = {};
function arrayToObject(arr) {
	// Code di sini
		function age(born){
		if (thisYear > born){
			var umur = thisYear - born;
		}else if (thisYear <= born || born == null){
			var umur = 'Invalid Birth Year'
		}
		return umur
	}
	
	if (arr == []){
	  var str = ""
	  console.log(str);
	}else{
	  var hasil = [];
	  var newObj = {};
	for (var i=0; i<arr.length; i++){
	  newObj[`${i+1}. ${arr[i][0]} ${arr[i][1]}`] = {
	    "firstName": arr[i][0],
  	    "lastName": arr[i][1],
  	    "gender": arr[i][2],
  	    "age": age(arr[i][3])
  	}
	}
	console.log(newObj)
	}
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""
console.log(" ");


//Soal No 2 (Shopping Time)
var barang = [
	'Sepatu Stacattu',
	'Baju Zoro',
	'Baju H&N',
	'Sweater Uniklooh',
	'Casing Handphone'
  ];
  var harga = [1500000,500000,250000,175000,50000];
  
  function shoppingTime(memberId, money) {
	  // you can only write your code here!
	  function changeMoney(money){
		var kembalian = money;
		for (var b=0; b<harga.length; b++){
		  if (kembalian >=harga[b]){
			kembalian -= harga[b];
		  }else if (kembalian < harga[b]){
			kembalian=kembalian;
		  }
		}
		return kembalian;
	  }
  
  var belanja = [];
	  function listPurchased(money){
		var uang = money;
		for (var x=0; x<barang.length; x++){
		  if (uang >= harga[x]){
			uang -= harga[x];
			belanja.push(barang[x]);
		  }else if (uang < harga[x]){
			uang=uang
		  }
		}
		return belanja;
	  }
	  
	  if (memberId == null || memberId == ''){
		var shop = "Mohon maaf, toko X hanya berlaku untuk member saja"
	  }else if (money <50000){
		var shop = "Mohon maaf, uang tidak cukup"
	  }else{
		var shop = {
		  memberId: memberId,
		  money: money,
		  listPurchased: listPurchased(money),
		  changeMoney: changeMoney(money)
		}
	  }
	  return shop;
  }
	 
	// TEST CASES
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
	  //{ memberId: '1820RzKrnWn08',
	  // money: 2475000,
	  // listPurchased:
	  //  [ 'Sepatu Stacattu',
	  //    'Baju Zoro',
	  //    'Baju H&N',
	  //    'Sweater Uniklooh',
	  //    'Casing Handphone' ],
	  // changeMoney: 0 }
	console.log(shoppingTime('82Ku8Ma742', 170000));
	//{ memberId: '82Ku8Ma742',
	// money: 170000,
	// listPurchased:
	//  [ 'Casing Handphone' ],
	// changeMoney: 120000 }
	console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
	console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
	console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja
	console.log(" ");


//Soal No 3 (Naik Angkot)
function naikAngkot(arrPenumpang) {
	rute = ['A', 'B', 'C', 'D', 'E', 'F'];
	hasil = [];
	//your code here
	for(var a=0; a<arrPenumpang.length; a++){
	 struk = {
	  penumpang: arrPenumpang[a][0],
	  naikDari: arrPenumpang[a][1],
	  tujuan: arrPenumpang[a][2],
	  bayar: (rute.indexOf(arrPenumpang[a][2])-rute.indexOf(arrPenumpang[a][1]))*2000
	 }
	 hasil.push(struk);
	}
	return hasil;
}
   
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
console.log(naikAngkot([])); //[]