// di index.js
var readBooks = require('./callback.js')
let myTime = 10000;
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini
function timeCount(num) {
    if(num == books.length){
        return 0;
    }else {
        readBooks(myTime, books[num], function(check){
            myTime = check;
            timeCount(num+1);
        })
    }
}
timeCount(0);