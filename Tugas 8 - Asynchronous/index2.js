var readBooksPromise = require('./promise.js')
let waktu = 10000;
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise
function timeCount(qty){
    if(qty == books.length)
    return 0;

    readBooksPromise(waktu, books[qty])
    .then(function(fulfilled){
        waktu = fulfilled;
        timeCount(qty+1);
    })
    .catch(function(rejected){

    })
}
timeCount(0);