//Soal 1
function teriak() {
  return "Halo Sanbers!"
};
console.log(teriak());
console.log(" ");

//Soal 2
var num1 = 12;
var num2 = 4;
function kalikan(a, b) {
  return a*b;
};
console.log(kalikan(num1, num2));
console.log(" ");

//Soal 3
var name = "Agus";
var age = 30;
var address = "Jln. Malioboro, Yogyakarta";
var hobby = "Gaming";

function introduce(nama, umur, alamat, hobi) {
  return ("Nama saya "+nama+", umur saya "+umur+" tahun, alamat saya di "+alamat+", dan saya punya hobby yaitu "+hobi+"!");
};

var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan);