// Nomor 1 (Mengubah fungsi menjadi fungsi arrow)
const golden = () => {
    console.log("this is golden!!")
}

golden()
console.log("\n");


// Nomor 2 (Sederhanakan menjadi Object literal di ES6)
const literal = (firstName, lastName) =>{
    return {
      firstName: firstName,
      lastName: lastName,
      fullName: () => {
        console.log(firstName + " " + lastName)
        return 
      }
    }
  }
  //Driver Code 
  literal("William", "Imoh").fullName()
  console.log("\n");



//Nomor 3 (Destructuring)
  const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }
  const {firstName, lastName, destination, occupation, spell} = newObject;
  console.log(firstName, lastName, destination, occupation);
  console.log("\n");



  //Nomor 4 (Array Spreading)
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east];
//Driver Code
console.log(combined)
console.log("\n");



//Nomor 5 (Template Literal)
const planet = "earth"
const view = "glass"
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`;
 
// Driver Code
console.log(before)